import {Component, OnInit} from '@angular/core';
import {RegisterRequestPayload} from "./register-request.payload";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../shared/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerRequestPayload: RegisterRequestPayload;
  registerForm: FormGroup;

  constructor(private authService: AuthService, private router: Router) {
    this.registerRequestPayload = {
      username: '',
      email: '',
      password: ''
    };
    this.registerForm = new FormGroup({
      username: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', Validators.required),
    });
  }

  ngOnInit(): void {
  }

  register() {
    this.registerRequestPayload.email = this.registerForm.get('email')?.value;
    this.registerRequestPayload.username = this.registerForm.get('username')?.value;
    this.registerRequestPayload.password = this.registerForm.get('password')?.value;

    this.authService.register(this.registerRequestPayload)
      .subscribe(data => {
        this.router.navigate(['/login'],
          {queryParams: {registered: 'true'}}).then(r => true);
      }, error => {
        console.log({error});
      });
  }
}
