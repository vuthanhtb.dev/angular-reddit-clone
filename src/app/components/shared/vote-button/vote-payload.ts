import {VoteType} from './vote-type';

export interface VotePayload {
  voteType: VoteType | undefined;
  postId: number | undefined;
}
