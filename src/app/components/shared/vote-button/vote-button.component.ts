import {Component, Input, OnInit} from '@angular/core';
import {throwError} from "rxjs";
import {faArrowDown, faArrowUp} from '@fortawesome/free-solid-svg-icons';
import {PostModel} from "../post-model";
import {VoteService} from "../vote.service";
import {AuthService} from "../../auth/shared/auth.service";
import {ToastrService} from "ngx-toastr";
import {PostService} from "../post.service";
import {VotePayload} from "./vote-payload";
import {VoteType} from "./vote-type";

@Component({
  selector: 'app-vote-button',
  templateUrl: './vote-button.component.html',
  styleUrls: ['./vote-button.component.css']
})
export class VoteButtonComponent implements OnInit {

  @Input() post: PostModel | undefined;
  votePayload: VotePayload;
  faArrowUp = faArrowUp;
  faArrowDown = faArrowDown;
  upvoteColor: string = '';
  downVoteColor: string = '';
  isLoggedIn: boolean = false;

  constructor(private voteService: VoteService,
              private authService: AuthService,
              private postService: PostService,
              private toastr: ToastrService) {

    this.votePayload = {
      voteType: undefined,
      postId: undefined
    }
    this.authService.loggedIn.subscribe((data: boolean) => this.isLoggedIn = data);
  }

  ngOnInit(): void {
    this.updateVoteDetails();
  }

  upvotePost() {
    this.votePayload.voteType = VoteType.UP_VOTE;
    this.vote();
    this.downVoteColor = '';
  }

  downVotePost() {
    this.votePayload.voteType = VoteType.DOWN_VOTE;
    this.vote();
    this.upvoteColor = '';
  }

  private vote() {
    this.votePayload.postId = this.post?.id;
    this.voteService.vote(this.votePayload).subscribe(() => {
      this.updateVoteDetails();
    }, error => {
      this.toastr.error(error.error.message);
      throwError(error);
    });
  }

  private updateVoteDetails() {
    this.postService.getPost(this.post?.id || -1).subscribe(post => {
      this.post = post;
    });
  }

}
