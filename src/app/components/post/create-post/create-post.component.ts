import {Component, OnInit} from '@angular/core';
import {throwError} from "rxjs";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {CreatePostPayload} from "./create-post.payload";
import {SubredditModel} from "../../subreddit/subreddit-response";
import {SubredditService} from "../../subreddit/subreddit.service";
import {Router} from "@angular/router";
import {PostService} from "../../shared/post.service";

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.css']
})
export class CreatePostComponent implements OnInit {

  createPostForm: FormGroup;
  postPayload: CreatePostPayload;
  subreddits: Array<SubredditModel> | undefined;

  constructor(private router: Router, private postService: PostService,
              private subredditService: SubredditService) {
    this.postPayload = {
      postName: '',
      url: '',
      description: '',
      subredditName: ''
    }

    this.createPostForm = new FormGroup({
      postName: new FormControl('', Validators.required),
      subredditName: new FormControl('', Validators.required),
      url: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
    });
  }

  ngOnInit() {

    this.subredditService.getAllSubreddits().subscribe((data) => {
      this.subreddits = data;
    }, error => {
      throwError(error);
    });
  }

  createPost() {
    this.postPayload.postName = this.createPostForm?.get('postName')?.value;
    this.postPayload.subredditName = this.createPostForm?.get('subredditName')?.value;
    this.postPayload.url = this.createPostForm?.get('url')?.value;
    this.postPayload.description = this.createPostForm?.get('description')?.value;

    this.postService.createPost(this.postPayload).subscribe((data) => {
      this.router.navigateByUrl('/').then(r => true);
    }, error => {
      throwError(error);
    })
  }

  discardPost() {
    this.router.navigateByUrl('/').then(r => true);
  }

}
